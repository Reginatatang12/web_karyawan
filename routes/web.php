<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//route CRUD
Route::get('/',[App\Http\Controllers\karyawanController::Class,'read'])->name('read');
Route::get('create',[App\Http\Controllers\karyawanController::Class,'create'])->name('create');
Route::post('store',[App\Http\Controllers\karyawanController::Class,'store'])->name('read');
Route::get('edit/{id}',[App\Http\Controllers\karyawanController::Class,'edit'])->name('read');
Route::post('update',[App\Http\Controllers\karyawanController::Class,'update'])->name('update');
Route::get('delete/{id}',[App\Http\Controllers\karyawanController::Class,'delete'])->name('delete');