
<!DOCTYPE html>
<html>
<head>
	<title>WEB KARYAWAN</title>
</head>
<body>

	<h2>UBAH DATA KARYAWAN</a></h2>
	<h3>Edit data karyawan</h3>

	<br/>

	@foreach($karyawan as $k)
	<form action="/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $k->id}}"> <br/>
		Nama 		:<input type="text" required="required" name="nama" value="{{ $k->nama_karyawan }}"> <br/><br/>
		No karyawan :<input type="text" required="required" name="no" value="{{ $k->no_karyawan }}"> <br/><br/>
		No Telp 	:<input type="tel" required="required" name="telp" value="{{ $k->no_telp_karyawan}}"> <br/><br/>
        Jabatan 	:<input type="text" required="required" name="jabatan" value="{{ $k->jabatan_karyawan}}"> <br/><br/>
		Divisi 		:<input type="text" required="required" name="divisi" value="{{ $k->divisi_karyawan }}"><br/><br/>
		<input type="submit" value="Simpan Data">
        <a href="/"> Kembali</a>
	</form>
	@endforeach
		
</body>
</html>