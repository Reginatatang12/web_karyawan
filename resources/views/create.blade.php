<!DOCTYPE html>
<html>
<head>
	<title>Data Karyawan Baru</title>
</head>
<body>
 
	<h2>SILAHKAN MASUKKAN DATA ANDA</h2>
	<h3>Data Karyawan Baru</h3>
	<br/>
 
	<form action="/store" method="post">
		{{csrf_field () }}
		Nama <input type="text" name="nama"> <br/><br/>
        No Karyawan <input type="text" name="no"> <br/><br/>
		No telp <input type="tel" id="telp" name="telp" required><br><br>
		Jabatan <input type="text" name="jabatan"> <br/><br/>
		Divisi : <br/>
        <input type="radio" name="divisi" value ="IT">
        <label for="IT">IT</label><br>
        <input type="radio" name="divisi" value ="Finance">
        <label for="Finance">Finance</label><br>
        <input type="radio" name="divisi" value ="Asset">
        <label for="Asset">Asset</label><br>
        <input type="radio" name="divisi" value ="Management">
        <label for="Management">Management</label><br>
        <input type="radio" name="divisi" value ="Marketing">
        <label for="Marketing">Marketing</label><br>
		<input type="submit" value="Simpan Data">
        <input type="reset" value="Hapus">
        <br/><a href="/"> Batal</a>
	</form>
		
</body>
</html>