<!DOCTYPE html>
<html>
<head>
	<title>WEB KARYAWAN </title>
</head>
<body>
 
	<h2>DATA BASE KARYAWAN</h2>
	<h3>Data Pegawai</h3>
 
	<a href="/create"> + Tambah Data karyawan Baru</a>
	
	<br/>
	<br/>

	<table border="2">
				<tr>
					<th>ID</th>
					<th>Nama</th>
					<th>No Karyawan</th>
					<th>No Telp</th>
					<th>Jabatan</th>
					<th>Divisi</th>
					<th>Setting</th>
				</tr>

				@foreach ($karyawan as $k)
				<tr>
					<td>{{$k->id}}</td>
					<td>{{$k->nama_karyawan}}</td>
					<td>{{$k->no_karyawan}}</td>
					<td>{{$k->no_telp_karyawan}}</td>
					<td>{{$k->jabatan_karyawan}}</td>
					<td>{{$k->divisi_karyawan}}</td>
					<td>
						<a href="/edit/{{$k->id}}">Edit</a>
						|
						<a href="/delete/{{$k->id}}">Hapus</a>
					</td>
				</tr>
				@endforeach
	</table>
</body>
</html>