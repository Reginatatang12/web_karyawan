<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class karyawanController extends Controller
{
    public function read()
    {
    	// mengambil data dari table karyawan
    	$karyawan = DB::table('karyawan')->get();
 
    	// mengirim data karyawan ke view read
    	return view('read',['karyawan' => $karyawan]);
    }

	// method untuk menampilkan view form tambah karyawan
	public function create()
	{
		// memanggil view tambah
		return view('create');
	}

	// method untuk insert data ke table karyawan
	public function store(Request $request)
	{
		// insert data ke table pegawai
		DB::table('karyawan')->insert([
			'nama_karyawan' => $request->nama,
			'no_karyawan' => $request->no,
			'no_telp_karyawan' => $request->telp,
			'jabatan_karyawan' => $request->jabatan,
			'divisi_karyawan' => $request->divisi
		]);
		// alihkan halaman ke halaman pegawai
		return redirect('/');
	}

	// method untuk edit data karyawan
	public function edit($id)
	{
		// mengambil data karyawan berdasarkan id yang dipilih
		$karyawan = DB::table('karyawan')->where('id',$id)->get();
		// passing data karyawan yang didapat ke view edit.blade.php
		return view('edit',['karyawan' => $karyawan]);
	}
 
	// update data karyawan
	public function update(Request $request)
	{
		DB::table('karyawan')->where('id',$request->id)->update([
			'nama_karyawan' => $request->nama,
			'no_karyawan' => $request->no,
			'no_telp_karyawan' => $request->telp,
			'jabatan_karyawan' => $request->jabatan,
			'divisi_karyawan' => $request->divisi
		]);
		// alihkan halaman ke halaman awal
		return redirect('/');
	}

	// method untuk hapus data karyawan
	public function delete($id)
	{
		// menghapus data karyawan berdasarkan id yang dipilih
		DB::table('karyawan')->where('id',$id)->delete();
		
		// dialihkan kembali ke halaman awal
		return redirect('/');
	}
}